import base64
import logging
import requests
import settings

log = logging.getLogger(__name__)

org_url = "https://gitee.com/api/v5/orgs"
repo_url = "https://gitee.com/api/v5/repos"
owner = "testPatchTrack"
token = settings.GITEE_ACCESS_TOKEN


def get_repo():
    '''
    获取指定组织下的所有仓库
    :param org_url:
    :param org:
    :param token:
    :return:
    '''
    url = '/'.join([org_url, owner, 'repos'])
    param = {'access_token': token}
    repo_list = list()

    r = requests.get(url, params=param)
    for item in r.json():
        repo_name = item['full_name'].split('/')[1]
        repo_list.append(repo_name)
    return repo_list


def get_branch(repo):
    '''
    获取指定仓库的所有分支
    :param repo_url:
    :param repos:
    :param token:
    :return:
    '''
    param = {'access_token': token}
    branch_list = list()
    url = '/'.join([repo_url, owner, repo, 'branches'])

    r = requests.get(url, params=param)
    for item in r.json():
        branch_list.append(item['name'])
    return branch_list


def get_yaml_content(repo, branch):
    '''
    获取指定仓库，指定分支的yaml文件内容
    :param repo_url:
    :param repo:
    :param branch:
    :param token:
    :return:
    '''
    yaml_content_dict = dict()
    path = repo + '.yaml'
    url = '/'.join([repo_url, owner, repo, 'contents', path])
    param = {'access_token': token, 'ref': branch}
    r = requests.get(url, params=param).json()
    content = str(base64.b64decode(r['content']), encoding='utf-8')
    for item in content.split('\n'):
        if item:
            key = item.split(':')[0].strip(' ')
            value = item.split(':')[1].strip(' ')
            yaml_content_dict[key] = value
    return yaml_content_dict


def post_create_branch(repo, branch, new_branch):
    '''
    创建分支
    :param repo_url:
    :param owner:
    :param repo:
    :param branch:
    :param new_branch:
    :param token:
    :return:
    '''
    url = '/'.join([repo_url, owner, repo, 'branches'])
    data = {
        'access_token': token,
        'refs': branch,
        'branch_name': new_branch
    }
    r = requests.post(url, data=data)
    if r.status_code == 201:
        return 'success'
    else:
        return r.json()


def post_upload_patch(repo, branch, scm_commit, last_scm_commit, patch_file_content):
    '''
    上传两个commit之间的patch文件
    :param repo_url:
    :param owner:
    :param repo:
    :param branch:
    :param patch_file_name:
    :param patch_file_content:
    :return:
    '''
    patch_file_name = scm_commit + '...' + last_scm_commit + '.patch'
    url = '/'.join([repo_url, owner, repo, 'contents', patch_file_name])
    content = base64.b64encode(patch_file_content.encode("utf-8"))
    data = {
        'access_token': token,
        'content': content,
        'message': 'upload patch file',
        'branch': branch
    }
    print(branch)
    r = requests.post(url, data=data)
    if r.status_code == 201:
        return 'success'
    else:
        return r.json()


def post_create_issue(repo, issue_body):
    '''
    创建issue
    :param repo_url:
    :param owner:
    :param repo:
    :param issue_body:
    :return:
    '''
    url = '/'.join([repo_url, owner, 'issues'])
    data = {
        'access_token': token,
        'repo': repo,
        'title': 'patch tracking',
        'body': issue_body
    }
    r = requests.post(url, data=data)
    if r.status_code == 201:
        return r.json()['number']
    else:
        return r.json()


def post_create_pull_request(repo, branch, patch_branch, issue_num):
    '''
    创建PR
    :param repo_url:
    :param owner:
    :param repo:
    :param branch:
    :param patch_branch:
    :param issue_num:
    :return:
    '''
    url = '/'.join([repo_url, owner, repo, 'pulls'])
    data = {
        'access_token': token,
        'repo': repo,
        'title': 'patch tracking',
        'head': patch_branch,
        'base': branch,
        'issue': issue_num
    }
    r = requests.post(url, data=data)
    if r.status_code == 201:
        return 'success'
    else:
        return r.json()
