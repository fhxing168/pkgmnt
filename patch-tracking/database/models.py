from database import db


class Tracking(db.Model):
    id = db.Column(db.Integer)
    version_control = db.Column(db.String(80))
    scm_repo = db.Column(db.String(80))
    scm_branch = db.Column(db.String(80))
    scm_commit = db.Column(db.String(80))
    repo = db.Column(db.String(80), primary_key=True)
    branch = db.Column(db.String(80), primary_key=True)
    enabled = db.Column(db.Boolean)

    def __init__(self, version_control, scm_repo, scm_branch, scm_commit, repo, branch, enabled=True):
        self.version_control = version_control
        self.scm_repo = scm_repo
        self.scm_branch = scm_branch
        self.scm_commit = scm_commit
        self.repo = repo
        self.branch = branch
        self.enabled = enabled

    def __repr__(self):
        return '<Tracking %r %r>' % (self.repo, self.branch)


class Issue(db.Model):
    issue = db.Column(db.String(80), primary_key=True)
    tracking = db.Column(db.Integer, db.ForeignKey('tracking.id'))
    track = db.relationship('Tracking', backref=db.backref('Issue'))

    def __init__(self, issue, tracking):
        self.issue = issue
        self.tracking = tracking

    def __repr__(self):
        return '<Issue %r %r>' % (self.issue, self.tracking)
