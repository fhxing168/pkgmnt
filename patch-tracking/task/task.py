from flask import current_app
import logging
from util.gitee_api import get_repo, get_branch, get_yaml_content, post_create_branch, post_upload_patch, \
    post_create_issue, post_create_pull_request
from util.github_api import GitHubApi
from database.models import Tracking, Issue
from api.business import create_tracking, update_tracking, delete_tracking, create_issue

log = logging.getLogger(__name__)

def get_yaml_info():
    '''
    从gitee托管的开源软件组织中获取所有的开源软件仓库里的yaml文件的信息
    :return:
    '''
    ret_lst = list()
    repo_list = get_repo()

    for repo in repo_list:
        branch_list = get_branch(repo)
        for branch in branch_list:
            ret_dict = dict()
            yaml_dict = get_yaml_content(repo, branch)
            ret_dict['scm_repo'] = yaml_dict['src_repo']
            ret_dict['scm_branch'] = yaml_dict['src_branch']
            ret_dict['repo'] = repo
            ret_dict['branch'] = branch
            if yaml_dict['patch_tracking_enabled'] == 'true':
                ret_dict['enabled'] = True
            else:
                ret_dict['enabled'] = False
            ret_lst.append(ret_dict)

    return ret_lst


def yaml_info_to_db(ret_lst):
    for item in ret_lst:
        data = Tracking.query.filter_by(repo=item['repo'], branch=item['branch']).first()
        if data:
            current_app.logger.info('update tracking: {}'.format(data))
            item['scm_commit'] = data.scm_commit
            update_tracking(item)
        else:
            github_api = GitHubApi()
            current_app.logger.info('create tracking: {}'.format(data))
            scm_commit = github_api.get_latest_commit(item['scm_repo'], item['scm_branch'])
            item['scm_commit'] = scm_commit[1]
            create_tracking(item)


def yaml_to_db():
    ret_lst = get_yaml_info()
    #ret_lst = [{'scm_repo': 'BJMX/testPatch01', 'scm_branch': 'master', 'scm_commit': 'bb4dcb0ef6c0c4e5de7a3ca58fb469bc6b791f03', 'repo': 'testPatch1', 'branch': 'newFeature', 'enabled': True}]
    yaml_info_to_db(ret_lst)


def db_to_patch():
    all_track = Tracking.query.filter_by(enabled=True)
    upload_patch_to_gitee(all_track)



def upload_patch_to_gitee(all_track):
    '''
    遍历Tracking数据表, 获取enabled仓的官方开源仓的patch文件，再将其提交pr和issue到对应的gitee仓库
    :return:
    '''
    patch_list = get_scm_patch(all_track)
    issue_list = upload_patch(patch_list)
    create_issue_db(issue_list)


def get_scm_patch(all_track):
    '''
    遍历Tracking数据表, 获取enabled仓的patch文件，不同的仓有不同的获取方式
    :return:
    '''
    scm_list = list()
    patch_list = list()
    github_api = GitHubApi()
    for item in all_track:
        if item.enabled is True:
            scm_dict = dict()
            scm_dict['scm_repo'] = item.scm_repo
            scm_dict['scm_branch'] = item.scm_branch
            scm_dict['scm_commit'] = item.scm_commit
            scm_dict['enabled'] = item.enabled
            scm_dict['repo'] = item.repo
            scm_dict['branch'] = item.branch
            scm_list.append(scm_dict)
    for scm in scm_list:
        status, result = github_api.get_latest_commit(scm['scm_repo'], scm['scm_branch'])
        if status == 'success':
            commitID = result
            if commitID != scm['scm_commit']:
                scm['last_scm_commit'] = commitID
                ret = github_api.get_patch(scm['scm_repo'], scm['scm_commit'], commitID)
                current_app.logger.info('get patch api ret ', ret)
                if ret['status'] == 'success':
                    scm['patch_content'] = ret['api_ret']
                    patch_list.append(scm)
                    data = {
                        'repo': scm['repo'],
                        'branch': scm['branch'],
                        'enabled': scm['enabled'],
                        'scm_commit': commitID,
                        'scm_branch': scm['scm_branch'],
                        'scm_repo': scm['scm_repo']
                    }
                    update_tracking(data)
                else:
                    current_app.logger.error('Error: github_api.get_patch failed. Repo: {}. Return val: {}'.format(scm, ret['api_ret']))
            else:
                current_app.logger.info('Info: github_api.get_patch nothing change. Repo: {}'.format(scm))
        else:
            current_app.logger.error('Error: github_api.get_latest_commit failed. Repo: {}. Return val: {}'.format(scm, result))

    return patch_list


def upload_patch(patch_list):
    '''
    创建临时分支，提交文件，创建PR和issue
    :return:
    '''
    issue_list = list()
    if patch_list != []:
        for patch in patch_list:
            issue_dict = dict()
            print(patch)

            repo = patch['repo']
            branch = patch['branch']
            issue_dict['repo'] = repo
            issue_dict['branch'] = branch
            new_branch = 'patch_tracking'
            patch_file_content = patch['patch_content']
            issue_body = 'patch file from commit {} to {}'.format(patch['scm_commit'], patch['last_scm_commit'])
            post_create_branch(repo, branch, new_branch)
            post_upload_patch(repo, branch, patch['scm_commit'], patch['last_scm_commit'], str(patch_file_content))
            issue_num = post_create_issue(repo, issue_body)
            post_create_pull_request(repo, branch, new_branch, issue_num)

            issue_dict['issue'] = issue_num
            issue_list.append(issue_dict)
    return issue_list


def create_issue_db(issue_list):
    for issue in issue_list:
        print(issue)
        issue_num = issue['issue']
        tracking = Tracking.query.filter_by(repo=issue['repo'], branch=issue['branch']).first()
        tracking_id = tracking.id
        data = {
            'issue': issue_num,
            'tracking': tracking_id
        }
        current_app.logger.info('issue data', data)
        create_issue(data)


if __name__ == '__main__':
    # get_patch()
    # test_db(app)
    pass
