# Flask settings
FLASK_SERVER_NAME = 'localhost:8888'
FLASK_DEBUG = False  # Do not use debug mode in production

# Flask-Restplus settings
RESTPLUS_SWAGGER_UI_DOC_EXPANSION = 'list'
RESTPLUS_VALIDATE = True
RESTPLUS_MASK_SWAGGER = False
RESTPLUS_ERROR_404_HELP = False

# SQLAlchemy settings
SQLALCHEMY_DATABASE_URI = 'sqlite:///db.sqlite'
SQLALCHEMY_TRACK_MODIFICATIONS = False

# GitHub API settings
GITHUB_ACCESS_TOKEN = '<GitHub token>'

# Gitee API settings
GITEE_ACCESS_TOKEN = '<Gitee token>'
